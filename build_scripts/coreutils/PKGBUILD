# Description: Utilities for showing and setting the basic system characteristics
# URL:         https://www.gnu.org/software/coreutils/
# Maintainer:  Emmett1, emmett1 dot 2miligrams at gmail dot com
# Depends on:  

pkgname=coreutils
pkgver=8.32
pkgrel=1
source=(https://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.xz
       coreutils-i18n-1.patch)

build() {
        cd $pkgname-$pkgver

        # fixes POSIX requires that programs from Coreutils recognize
        # character boundaries correctly even in multibyte locales
        #patch -Np1 -i ../coreutils-i18n-1.patch
        sed -i '/test.lock/s/^/#/' gnulib-tests/gnulib.mk

        autoreconf -fiv
        FORCE_UNSAFE_CONFIGURE=1 \
        ./configure \
                --prefix=/usr \
                --enable-no-install-program=kill,uptime
        make
        make DESTDIR=$pkgdir install

	mkdir -pv $pkgdir/{bin,usr/sbin}
        mv -v $pkgdir/usr/bin/{cat,chgrp,chmod,chown,cp,date,dd,df,echo} $pkgdir/bin
        mv -v $pkgdir/usr/bin/{false,ln,ls,mkdir,mknod,mv,pwd,rm} $pkgdir/bin
        mv -v $pkgdir/usr/bin/{rmdir,stty,sync,true,uname} $pkgdir/bin
        mv -v $pkgdir/usr/bin/chroot $pkgdir/usr/sbin
        mkdir -pv $pkgdir/usr/share/man/man8
        mv -v $pkgdir/usr/share/man/man1/chroot.1 $pkgdir/usr/share/man/man8/chroot.8
        sed -i s/\"1\"/\"8\"/1 $pkgdir/usr/share/man/man8/chroot.8

        mv -v $pkgdir/usr/bin/{head,sleep,nice} $pkgdir/bin
}
